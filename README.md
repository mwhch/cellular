# Simulate cellular automatons
For further information on cellular automatons, like the convention to assign [rules](http://www.conwaylife.com/wiki/Rulestring), go visit [http://www.conwaylife.com/wiki/Main_Page](http://www.conwaylife.com/wiki/Main_Page)

## Requirements: [sfml](https://www.sfml-dev.org/)

### usage:
(if arguments are not specified, the default values will be used)

`./compiled-file window_width window_height grid_width grid_height delay probability birth_rules survive_rules`

* window_width, window_height (int): window size in pixels (default: 600,600)
* grid_width, grid_height (int): amount of cells in x and y direction (default: 100,100)
* delay, probability (float): delay in ms, probability in %/100 (0.2=20%) (default: 100,0.1)
* birth_rules (str): the number of living cells a dead cell needs in order to come alive (default: 23)
* survive_rules (str): the number of living cells a alive cell needs in order to stay alive (default: 3)