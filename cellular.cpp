#include <iostream>
#include <string>
#include <string.h>
#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>

using namespace std;

int main(int argc, char *argv[]) {
    cout << "order of arguments: ";
    cout << "int window_width, int window_height, ";
    cout << "int grid_width, int grid_height, ";
    cout << "float delay(in ms), float probabiilty, ";
    cout << "(numerical) str birth_rules, (numerical) str survive_rules";
    cout << endl;
    int window_width_index = 1;
    int window_height_index = window_width_index+1;
    int grid_width_index = window_height_index+1;
    int grid_height_index = grid_width_index+1;
    int delay_index = grid_height_index+1;
    int probability_index=delay_index+1;
    int birth_rules_index=probability_index+1;
    int survival_rules_index=birth_rules_index+1;

    bool done = false;
    float delay_ms = 100;
    if(argc >= delay_index) {
        delay_ms = stoi(argv[delay_index]);
    }
    float probability = 0.1;
    if(argc >= probability_index) {
        probability = stof(argv[probability_index]);
    }
    // initialize rules
    //  birth rules
    string birth_rules_str = "23";
    if(argc >= birth_rules_index) {
        birth_rules_str = string(argv[birth_rules_index]);
    }
    int birth[(unsigned)strlen(birth_rules_str.c_str())];
    for(int i = 0; i < (unsigned)strlen(birth_rules_str.c_str()); i++) {
        birth[i] = (int)(birth_rules_str[i])-48;
    }
    //  survival rules
    string survival_rules_str = "3";
    if(argc >= survival_rules_index) {
        survival_rules_str = string(argv[survival_rules_index]);
    }
    int survive[(unsigned)strlen(survival_rules_str.c_str())];
    for(int i = 0; i < (unsigned)strlen(survival_rules_str.c_str()); i++) {
        survive[i] = (int)(survival_rules_str[i]) - 48; //substract 48 to convert the digit from ascii to base10
    }
    // initialize window
    int window_width = 600;
    int window_height= 600;
    if(argc >= window_width_index && argc >= window_height_index) {
        window_width = stoi(argv[window_width_index]);
        window_height = stoi(argv[window_height_index]);
    }
    sf::RenderWindow window(sf::VideoMode(window_width, window_height), "cellular");
    // initialize grid and cells
    int grid_width = 100;
    int grid_height= 100;
    if(argc >= grid_height_index && argc >= grid_width_index) {
        grid_width = stoi(argv[grid_width_index]);
        grid_height= stoi(argv[grid_height_index]);
    }
    int cell_width=window_width/grid_width;
    int cell_height=window_height/grid_height;
    sf::Color background_color = sf::Color::Black;
    sf::Color cell_color = sf::Color::White;
    bool grid[grid_height][grid_width];
    srand(time(NULL));
    sf::RectangleShape rectangle_grid[grid_height][grid_width];
    window.clear(background_color);
    for(int y=0; y < grid_height; y++) {
        for(int x=0; x < grid_width; x++) {
            grid[y][x] = (rand()%100+1) <= probability*100;
            rectangle_grid[y][x].setPosition(sf::Vector2f(x*cell_width, y*cell_height));
            rectangle_grid[y][x].setSize(sf::Vector2f(cell_width, cell_height));
            rectangle_grid[y][x].setOutlineColor(background_color);
            rectangle_grid[y][x].setOutlineThickness(1);
            if(grid[y][x]) {
                rectangle_grid[y][x].setFillColor(cell_color);
            } else {
                rectangle_grid[y][x].setFillColor(background_color);
            }
            window.draw(rectangle_grid[y][x]);
        }
    }
    window.display();
    
    sf::Clock clock;
    while(!done) {
        sf::Event event;
        while(window.pollEvent(event)) {
            if(event.type == sf::Event::Closed) {
                done = true;
                window.close();
            }
        }
        clock.restart();
        // main loop
        window.clear(background_color);
        for(int y = 0; y < grid_height; y++) {
            for(int x = 0; x < grid_width; x++) {
                int min_range_y = -1;
                int max_range_y = 1;
                int min_range_x = -1;
                int max_range_x = 1;
                if(y == 0)
                    min_range_y = 0;
                if(y == grid_height - 1)
                    max_range_y = 0;
                if(x == 0)
                    min_range_x = 0;
                if(x == grid_width -1)
                    max_range_x = 0;
                    
                int amount_of_neighbours = 0;
                for(int i = min_range_y; i <= max_range_y; i++) {
                    for(int j = min_range_x; j <= max_range_x; j++) {
                        if((y+i == y && x+j == x)) {
                        } else {
                            if(grid[y+i][x+j] == true) {
                                amount_of_neighbours++;
                            }
                        }
                    }
                }
                if(grid[y][x]) {
                    bool survives = false;
                    for(int i = 0; i < survival_rules_str.length(); i++) {
                        if(amount_of_neighbours == survive[i]) {
                            survives=true;
                        }
                    }
                    grid[y][x] = survives;
                } else {
                    bool is_born = false;
                    for(int i = 0; i < birth_rules_str.length(); i++) {
                        if(amount_of_neighbours == birth[i]) {
                            grid[y][x] = true;
                        }
                    }
                } 

                if(grid[y][x]) {
                    rectangle_grid[y][x].setFillColor(cell_color);
                } else {
                    rectangle_grid[y][x].setFillColor(background_color);
                }
                window.draw(rectangle_grid[y][x]);
            }
        }
        sf::Time time_elapsed = clock.getElapsedTime();
        while(time_elapsed.asMilliseconds() < delay_ms) {
            time_elapsed = clock.getElapsedTime();
        }
        window.display();
    }
    return EXIT_SUCCESS;
}